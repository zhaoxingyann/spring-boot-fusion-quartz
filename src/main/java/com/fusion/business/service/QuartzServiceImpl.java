package com.fusion.business.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fusion.business.job.TestJob;
import com.fusion.common.tools.CronTool;
import com.fusion.common.tools.StringTool;

import cn.hutool.core.lang.UUID;

@Service
public class QuartzServiceImpl {
	
	private String jobName="jobName01";
	private String jobGroup="jobGroup01";
	private String triggerName="triggerName01";
	private String triggerGroup="triggerGroup01";

	@Autowired
	private Scheduler scheduler;

    /**
     * 描述: 添加任务
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public void addJob() {
		JobBuilder jobBuilder = JobBuilder.newJob(TestJob.class);
		jobBuilder.withIdentity(jobName, jobGroup);
		JobDetail jobDetail = jobBuilder.build();
		
		TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
		triggerBuilder.withIdentity(triggerName, triggerGroup);
	    // 开始时间，结束时间
		triggerBuilder.startAt(StringTool.stringToDate("2024-01-26 00:00:00"));
		triggerBuilder.endAt(StringTool.stringToDate("2100-01-26 00:00:00"));
		// 冗余参数
		triggerBuilder.usingJobData("abc", "123456");
		triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule("*/10 * * * * ? *"));
		Trigger trigger = triggerBuilder.build();
		try {
			scheduler.scheduleJob(jobDetail, trigger);
			scheduler.start();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
    /**
     * 描述: 停止任务
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public void stopJob() {
		try {
			scheduler.pauseTrigger(TriggerKey.triggerKey(triggerName, triggerGroup));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
    /**
     * 描述: 恢复任务
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public void recoverJob() {
		try {
			scheduler.resumeTrigger(TriggerKey.triggerKey(triggerName, triggerGroup));
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
    /**
     * 描述: 删除任务
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public void deleteJob() {
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroup);
			scheduler.pauseTrigger(triggerKey);//暂停触发器
			scheduler.unscheduleJob(triggerKey);//移除触发器
			scheduler.deleteJob(JobKey.jobKey(jobName, jobGroup));//删除Job
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
    /**
     * 描述: 获取下一次执行时间
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public String nextTime() {
		Date nextFireTime = null;
        try {
            Trigger trigger = scheduler.getTrigger(TriggerKey.triggerKey(triggerName, triggerGroup));
            nextFireTime = trigger.getNextFireTime();
        } catch (Exception exception) {
			exception.printStackTrace();
		}
        return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(nextFireTime);
    }
	
    /**
     * 描述: 获取下一次执行时间
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public List<Date> nextTime(String cron,String startTime,String endTime,int num) {
		return CronTool.nextTime(cron, startTime, endTime, num);
    }
	
    /**
     * 描述: 执行一次
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public void runOne() {
		 try {
			String temporaryTaskValue = UUID.randomUUID().toString();
			JobBuilder jobBuilder = JobBuilder.newJob(TestJob.class);
			jobBuilder.withIdentity(jobName, temporaryTaskValue);
			JobDetail jobDetail = jobBuilder.build();
			TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
			triggerBuilder.withIdentity(temporaryTaskValue, temporaryTaskValue);
			triggerBuilder.usingJobData("jobType", temporaryTaskValue);
			triggerBuilder.startAt(new Date());
			triggerBuilder.withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(10).withRepeatCount(0));
			Trigger trigger = triggerBuilder.build();
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
}
