package com.fusion.business.controller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fusion.business.service.QuartzServiceImpl;

@RestController
public class TestController {
	
	@Autowired
	private QuartzServiceImpl quartzService;

	@GetMapping("test")
	public Object test() {
		return UUID.randomUUID().toString();
	}
	
	@GetMapping("add")
	public Object add() {
		quartzService.addJob();
		return UUID.randomUUID().toString();
	}
	
	@GetMapping("stop")
	public Object stop() {
		quartzService.stopJob();
		return UUID.randomUUID().toString();
	}
	
	@GetMapping("recover")
	public Object recover() {
		quartzService.recoverJob();
		return UUID.randomUUID().toString();
	}
	
	@GetMapping("delete")
	public Object delete() {
		quartzService.deleteJob();
		return UUID.randomUUID().toString();
	}
	
	@GetMapping("nextTime")
	public Object nextTime() {
		return quartzService.nextTime();
	}
	
	@GetMapping("runOne")
	public Object runOne() {
		quartzService.runOne();
		return UUID.randomUUID().toString();
	}

}
