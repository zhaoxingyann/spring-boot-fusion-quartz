package com.fusion.business.other;

import java.util.List;
import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

public class OtherApi {
	
	 public static void main(String[] args) throws Exception {
		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
		
		/**
		 * 查看任务的详情
		 */
		 JobKey jobKey = JobKey.jobKey("jobName01", "jobGroup01");
		 JobDetail jobDetail = scheduler.getJobDetail(jobKey);
		 System.out.println(jobDetail);
		 
		 /**
		  * 查询所有任务
		  */
		 List<String> triggerGroupNames = scheduler.getTriggerGroupNames();
		 for (String triggerGroupName : triggerGroupNames) {
			  Set<TriggerKey> triggerKeySet = scheduler.getTriggerKeys(GroupMatcher.triggerGroupEquals(triggerGroupName));
			  for (TriggerKey triggerKey : triggerKeySet) {
				   Trigger trigger = scheduler.getTrigger(triggerKey);
				   JobKey jobKey2 = trigger.getJobKey();
				   System.out.println(jobKey2);
			  }
		 }
		 
		 /**
		  * 判断任务是否存在
		  */
		 if (scheduler.checkExists(jobKey)) {
			 System.out.println(scheduler.checkExists(jobKey));
		 }
		 
		 /**
		  * 设置任务的错过机制
		  */
		 TriggerKey triggerKey = TriggerKey.triggerKey("triggerName01", "triggerGroup01");
		 TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
		 triggerBuilder.withIdentity(triggerKey);
		 triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule("*/5 * * * * ? *").withMisfireHandlingInstructionDoNothing());
		 
		 /**
		  * 获取任务状态
		  */
		 TriggerState triggerState = scheduler.getTriggerState(triggerKey);
		 System.out.println(triggerState);
	}

}
