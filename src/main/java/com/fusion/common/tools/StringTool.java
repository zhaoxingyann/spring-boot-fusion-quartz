package com.fusion.common.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 描述: 工具类
 * 创建人: 赵兴炎
 * 日期: 2022年9月13日
 */
public class StringTool {
	
	
	/**
	 * 描述: 字符串转日期
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月13日
	 */
	public static Date stringToDate(String dateTime) {
		Date dateTimeValue = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			dateTimeValue = simpleDateFormat.parse(dateTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateTimeValue;
	}
	
	/**
	 * 描述: 日期转字符串
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月13日
	 */
	public static String dateToString(Date dateTime) {
		String resultData = null;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			resultData = simpleDateFormat.format(dateTime);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return resultData;
	}
	
}
