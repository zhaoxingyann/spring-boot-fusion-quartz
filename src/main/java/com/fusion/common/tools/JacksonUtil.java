package com.fusion.common.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * 描述: Json工具类
 * 创建人: 赵兴炎
 * 日期: 2022年9月5日
 */
public final class JacksonUtil {

	/**
	 * 设置过滤掉不匹配的字段
	 */
	private static ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	
	/**
	 * 处理LocalDate的序列化问题
	 */
	static {
		JavaTimeModule javaTimeModule = new JavaTimeModule();
		mapper.registerModule(javaTimeModule);
	}
	/**
	 * 描述: Json转Map，不指定泛型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static Map<?, ?> JsonToHashMap(String jsonString) {
		HashMap<?, ?> map = null;
		try {
			map = mapper.readValue(jsonString, HashMap.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 描述: Json转Map，需指定KEY和VALUE的类型
	 * @param jsonString：json字符串
	 * @param key：KEY的类型
	 * @param value：VALUE的类型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static <K, V> Map<K, V> JsonToHashMap(String jsonString, Class<K> key, Class<V> value) {
		HashMap<K, V> mapValue = null;
		try {
			JavaType mapType = getHashMapType(key, value);
			mapValue = mapper.readValue(jsonString, mapType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapValue;
	}

	/**
	 * 描述: 数据转成Json字符串
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static String ObjectToJson(Object object) {
		String writeValueAsString = null;
		try {
			writeValueAsString = mapper.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return writeValueAsString;
	}
	
	/**
	 * 描述: Json字符串转换集合
	 * @param jsonString：json字符串
	 * @param clazz：集合泛型类型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static <T> List<T> jsonToList(String jsonString, Class<T> clazz) {
		JavaType javaType = getCollectionType(ArrayList.class, clazz);
		List<T> readValue = null;
		try {
			readValue = mapper.readValue(jsonString, javaType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readValue;
	}

	/**
	 * 描述: Json字符串转对象
	 * @param jsonString：json字符串
	 * @param clazz：实体类型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static <T> T jsonToClass(String jsonString, Class<T> clazz) {
		T readValue = null;
		try {
			readValue = mapper.readValue(jsonString, clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return readValue;
	}

	/**
	 * 描述: 实体转实体
	 * @param bean：需要转换的实体
	 * @param clazz：转换后的实体类型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static <T> T classToClass(Object bean, Class<T> clazz) {
		String objectToJson = ObjectToJson(bean);
		T jsonToClass = jsonToClass(objectToJson, clazz);
		return jsonToClass;
	}

	/**
	 * 描述: 集合转集合
	 * @param object：需要转换的集合
	 * @param clazz：转换后的集合泛型
	 * 创建人: 赵兴炎
	 * 日期: 2022年9月5日
	 */
	public static <T> List<T> ListToList(Object object, Class<T> clazz) {
		String objectToJson = ObjectToJson(object);
		return jsonToList(objectToJson, clazz);
	}

	public static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
		return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
	}

	public static JavaType getHashMapType(Class<?> keyClass, Class<?> valueClass) {
		return mapper.getTypeFactory().constructMapLikeType(HashMap.class, keyClass, valueClass);
	}

	public static void main(String[] args) {
		HashMap<String, String> hashMap = new HashMap<>();
		hashMap.put("a", "123");
		hashMap.put("b", "890");
		String objectToJson = JacksonUtil.ObjectToJson(hashMap);
		System.out.println(objectToJson);

		Map<String, String> jsonToMap = JacksonUtil.JsonToHashMap(objectToJson, String.class, String.class);
		System.out.println(jsonToMap);
	}

}