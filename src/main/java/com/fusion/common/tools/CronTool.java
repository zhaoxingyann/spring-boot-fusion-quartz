package com.fusion.common.tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.quartz.TriggerUtils;
import org.quartz.impl.triggers.CronTriggerImpl;
import cn.hutool.cron.pattern.CronPatternBuilder;
import cn.hutool.cron.pattern.CronPatternUtil;
import cn.hutool.cron.pattern.Part;

public class CronTool {
	
    /**
     * 描述: 获取任务执行表达书
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public static void getJobCron() {
		CronPatternBuilder of = CronPatternBuilder.of();
		of.set(Part.SECOND,"0");
		of.set(Part.MINUTE,"0");
		of.set(Part.HOUR,"3");
//		of.set(Part.DAY_OF_MONTH,"*");
		of.set(Part.DAY_OF_WEEK,"1,3");
		of.set(Part.MONTH,"3");
//		of.set(Part.YEAR,"*");
		String build = of.build();
		System.out.println(build);
		List<Date> matchedDates = CronPatternUtil.matchedDates(build, new Date(), 5, true);
		for (Date date : matchedDates) {
			 System.out.println(StringTool.dateToString(date));
		}
	}
	
    /**
     * 描述: 获取执行时间列表
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public static List<Date> nextTime(String cron,int num) {
		List<Date> nextTimeList = new ArrayList<>();
		try {
			CronTriggerImpl cronTriggerImpl = new CronTriggerImpl();
			cronTriggerImpl.setCronExpression(cron);
			List<Date> computeFireTimesBetween = TriggerUtils.computeFireTimes(cronTriggerImpl, null,10000);
			if(computeFireTimesBetween!=null && computeFireTimesBetween.size() > 0) {
				for (int i = 0; i < computeFireTimesBetween.size(); i++) {
					 Date computeFireTimesDate = computeFireTimesBetween.get(i);
					 long computeFireTimesTime = computeFireTimesDate.getTime();
					 if(computeFireTimesTime>=new Date().getTime())nextTimeList.add(computeFireTimesDate);
					 if(nextTimeList.size() == num) break;
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return nextTimeList;
	}
	
    /**
     * 描述: 获取执行时间列表
     * 创建人: 赵兴炎
     * 日期: 2023年05月23日
     */
	public static List<Date> nextTime(String cron,String startTime,String endTime,int num) {
		List<Date> nextTimeList = new ArrayList<>();
		try {
			Date stringToDateStart = StringTool.stringToDate(startTime);
			Date stringToDateEnd = StringTool.stringToDate(endTime);
			CronTriggerImpl cronTriggerImpl = new CronTriggerImpl();
			cronTriggerImpl.setCronExpression(cron);
			List<Date> computeFireTimesBetween = TriggerUtils.computeFireTimesBetween(cronTriggerImpl, null, stringToDateStart,stringToDateEnd);
			if(computeFireTimesBetween!=null && computeFireTimesBetween.size() > 0) {
				for (int i = 0; i < computeFireTimesBetween.size(); i++) {
					 Date computeFireTimesDate = computeFireTimesBetween.get(i);
					 long computeFireTimesTime = computeFireTimesDate.getTime();
					 if(computeFireTimesTime>=new Date().getTime())nextTimeList.add(computeFireTimesDate);
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return nextTimeList;
	}
	
	public static void main(String[] args) throws Exception {
		List<Date> nextTime = nextTime("0 0/5 * * * ?",1);
		for (Date date : nextTime) {
			 System.out.println(StringTool.dateToString(date));
		}
	}

}
